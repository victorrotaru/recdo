package com.victor.recdo.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.victor.recdo.database.Note;

abstract class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
    private final OnItemClickListener mListener;
    private GestureDetector mGestureDetector;
    private Note mNote;

    RecyclerItemClickListener(Context context, final RecyclerView view, OnItemClickListener listener, Note mNote) {
        mListener = listener;
        this.mNote = mNote;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                View childView = view.findChildViewUnder(e.getX(), e.getY());
                if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                    mListener.onItemClick(RecyclerItemClickListener.this.mNote);
                }
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull RecyclerView view, @NonNull MotionEvent e) {
        if (mGestureDetector != null) {
            return mGestureDetector.onTouchEvent(e);
        } else {
            return false;
        }
    }

    interface OnItemClickListener {
        void onItemClick(Note note);
    }
}
