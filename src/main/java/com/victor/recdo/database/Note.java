package com.victor.recdo.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "note_table")
public class Note {

    @ColumnInfo
    private final String mNote;
    @ColumnInfo
    private final String mRecord;
    @ColumnInfo
    private final String mImage;
    @ColumnInfo
    private final String mReminder;
    @PrimaryKey(autoGenerate = true)
    private int id;


    public Note(String mNote, String mRecord, String mImage, String mReminder) {
        this.mNote = mNote;
        this.mRecord = mRecord;
        this.mImage = mImage;
        this.mReminder = mReminder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return mNote;
    }

    public String getRecord() {
        return mRecord;
    }

    public String getImage() {
        return mImage;
    }

    public String getReminder() {
        return mReminder;
    }
}
