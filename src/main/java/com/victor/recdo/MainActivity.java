package com.victor.recdo;

import android.Manifest;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import com.victor.recdo.adapters.NoteAdapter;
import com.victor.recdo.database.Note;
import com.victor.recdo.database.NoteViewModel;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    public static final String EXTRA_ID = "com.victor.recdo.EXTRA_ID";
    public static final String EXTRA_NOTE = "com.victor.recdo.EXTRA_NOTE";
    public static final String EXTRA_RECORD = "com.victor.recdo.EXTRA_RECORD";
    public static final String EXTRA_IMAGE = "com.victor.recdo.EXTRA_IMAGE";
    public static final String EXTRA_REMINDER = "com.victor.recdo.EXTRA_REMINDER";
    private static final int ADD_NOTE_REQUEST = 1;
    private static final int EDIT_NOTE_REQUEST = 2;
    public static final int REQUEST_RECORD = 1222;
    public static final int REQUEST_STORAGE = 1233;
    private static final String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    private NoteViewModel noteViewModel;
    private RecyclerView recyclerView;
    private NoteAdapter adapter;
    private View mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        mLayout = findViewById(R.id.main_layout);
        myToolbar.setTitle("");
        setSupportActionBar(myToolbar);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
        }

        fabBtn();
        noteRecDec();
    }

    private void noteRecDec() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        adapter = new NoteAdapter();
        recyclerView.setAdapter(adapter);

        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        noteViewModel.getAllNotes().observe(this, notes -> adapter.setNotes(notes));
        modelTouchHelper();
        adapterOnItemClick();
    }

    private void modelTouchHelper() {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                noteViewModel.delete(adapter.getNoteAt(viewHolder.getAdapterPosition()));
            }
        }).attachToRecyclerView(recyclerView);
    }

    private void adapterOnItemClick() {
        adapter.setOnItemClickListener(note -> {
            Intent intent = new Intent(MainActivity.this, AddEditNote.class);
            intent.putExtra(EXTRA_ID, note.getId());
            intent.putExtra(EXTRA_NOTE, note.getNote());
            intent.putExtra(EXTRA_RECORD, note.getRecord());
            intent.putExtra(EXTRA_IMAGE, note.getImage());
            intent.putExtra(EXTRA_REMINDER, note.getReminder());
            startActivityForResult(intent, EDIT_NOTE_REQUEST);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_NOTE_REQUEST && resultCode == RESULT_OK) {
            String note = data.getStringExtra(EXTRA_NOTE);
            String record = data.getStringExtra(EXTRA_RECORD);
            String image = data.getStringExtra(EXTRA_IMAGE);
            String reminder = data.getStringExtra(EXTRA_REMINDER);
            Note noteObj = new Note(note, record, image, reminder);
            noteViewModel.insert(noteObj);
        } else if (requestCode == EDIT_NOTE_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(EXTRA_ID, -1);
            if (id == -1) {
                Toast.makeText(this, "Can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }
            String note = data.getStringExtra(EXTRA_NOTE);
            String record = data.getStringExtra(EXTRA_RECORD);
            String image = data.getStringExtra(EXTRA_IMAGE);
            String reminder = data.getStringExtra(EXTRA_REMINDER);
            Note noteObj = new Note(note, record, image, reminder);
            noteObj.setId(id);
            noteViewModel.update(noteObj);
        }
    }

    private void fabBtn() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, AddEditNote.class);
            startActivityForResult(intent, ADD_NOTE_REQUEST);
        });
    }

    private void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Snackbar.make(mLayout, R.string.all_permissions,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, view -> ActivityCompat
                            .requestPermissions(MainActivity.this, PERMISSIONS,
                                    REQUEST_STORAGE))
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_RECORD) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Snackbar.make(mLayout, R.string.permission_available_record,
                        Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(mLayout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, v -> openSettings())
                        .show();
            }
        } else if (requestCode == REQUEST_STORAGE) {
            if (PermissionUtil.verifyPermissions(grantResults)) {
                Snackbar.make(mLayout, R.string.permission_available_storage,
                        Snackbar.LENGTH_SHORT)
                        .show();
            } else {
                Snackbar.make(mLayout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, v -> openSettings())
                        .show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

}