package com.victor.recdo.reminder;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.victor.recdo.AddEditNote;
import com.victor.recdo.R;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Objects;

@SuppressLint("StaticFieldLeak")
public class ReminderSheet extends BottomSheetDialogFragment implements View.OnClickListener {
    public static final String ARG_REMINDER = "com.victor.recdo.reminder.ARG_REMINDER";
    static TextView sTv_set_time;
    static TextView sTv_set_date;
    static int sYear;
    static int sMonth;
    static int sDay;
    static int sHour;
    static int sMinute;
    private static Button sSetTime;
    private static Button sSetDate;
    private ReminderInterface mReminderCallBack;
    private Switch mSwitchButton;
    private String mGetArgReminder;
    private Calendar cal = Calendar.getInstance();

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reminder_sheet, container, false);
        init(view);
        reminderSwitcher();
        Bundle bundle = this.getArguments();
        assert bundle != null;
        mGetArgReminder = bundle.getString(ARG_REMINDER);
        if (mGetArgReminder != null) {
            mSwitchButton.setChecked(true);
            sSetTime.setText(getString(R.string.update_time));
            sSetDate.setText(R.string.update_date);
            long argLong = Long.parseLong(mGetArgReminder);
            cal.setTimeInMillis(argLong);
            sYear = cal.get(Calendar.YEAR);
            sMonth = cal.get(Calendar.MONTH);
            sDay = cal.get(Calendar.DAY_OF_MONTH);
            sHour = cal.get(Calendar.HOUR_OF_DAY);
            sMinute = cal.get(Calendar.MINUTE);

            sTv_set_time.setText(sHour + ":" + sMinute + ", ");
            sTv_set_date.setText(sDay + "/" + (sMonth + 1) + "/" + sYear);
        } else {
            mSwitchButton.setChecked(false);
        }

        return view;
    }

    private void init(@NotNull View view) {
        sSetTime = view.findViewById(R.id.btn_time);
        sSetTime.setOnClickListener(this);
        sSetTime.setEnabled(false);
        sSetDate = view.findViewById(R.id.btn_date);
        sSetDate.setOnClickListener(this);
        sSetDate.setEnabled(false);
        mSwitchButton = view.findViewById(R.id.reminderSwitch);
        sTv_set_time = view.findViewById(R.id.tv_set_time);
        sTv_set_date = view.findViewById(R.id.tv_set_date);
    }

    private void reminderSwitcher() {
        mSwitchButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                sSetTime.setEnabled(true);
                sSetDate.setEnabled(true);
            } else {
                sSetTime.setEnabled(false);
                sSetDate.setEnabled(false);
                sTv_set_time.setText("");
                sTv_set_date.setText("");
                sSetTime.setText(getString(R.string.set_time));
                sSetDate.setText(getString(R.string.set_date));
            }
        });
    }

    private void startAlarm(@NotNull Calendar c) {
        AlarmManager alarmManager = (AlarmManager) Objects.requireNonNull(getActivity()).getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getActivity(), AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1, intent, 0);

        if (c.before(Calendar.getInstance())) {
            c.add(Calendar.DATE, 1);
        }

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
    }

    private void cancelAlarm() {
        AlarmManager alarmManager = (AlarmManager) Objects.requireNonNull(getActivity()).getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getActivity(), AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1, intent, 0);

        alarmManager.cancel(pendingIntent);
    }

    @Override
    public void onClick(@NotNull View v) {
        switch (v.getId()) {
            case R.id.btn_time:
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "time picker");
                break;
            case R.id.btn_date:
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "date picker");
                break;
            default:
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mSwitchButton.isChecked()) {
            if (cal.before(Calendar.getInstance())) {
                startAlarm(cal);
                mReminderCallBack.mDateTimeSender(String.valueOf(cal.getTimeInMillis()));
            } else {
                Toast.makeText(getActivity(), "Invalid reminder", Toast.LENGTH_SHORT).show();
                ((AddEditNote) Objects.requireNonNull(getActivity())).mShowReminder.setText("Reminder set for tralalaalalalalal");
            }

        } else {
            cancelAlarm();
        }
    }

    public void setReminderCallBack(ReminderInterface mReminderCallBack) {
        this.mReminderCallBack = mReminderCallBack;
    }

    public interface ReminderInterface {
        void mDateTimeSender(String dateTime);
    }
}
