package com.victor.recdo.mediaHelper;

import android.media.MediaRecorder;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.UUID;

public class MediaRecorderHelper {

    private final String mSavePath;
    private MediaRecorder mMediaRecorder;
    private String mCurrentFilePath;

    public MediaRecorderHelper(String savePath) {
        mSavePath = savePath;
        File file = new File(mSavePath);
        if (!file.exists()) file.mkdirs();
    }

    public void startRecord() {
        try {
            mMediaRecorder = new MediaRecorder();
            File file = new File(mSavePath, generateFileName());
            mCurrentFilePath = file.getAbsolutePath();
            mMediaRecorder.setOutputFile(mCurrentFilePath);
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mMediaRecorder.prepare();
            mMediaRecorder.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopAndRelease() {
        if (mMediaRecorder == null) return;
        try {
            mMediaRecorder.stop();
        } catch (RuntimeException stopException) {
            // handle clean up here
        }
        mMediaRecorder.release();
        mMediaRecorder = null;
    }

    @NonNull
    private String generateFileName() {
        return UUID.randomUUID().toString() + ".3gp";
    }

    public String getCurrentFilePath() {
        return mCurrentFilePath;
    }

}

