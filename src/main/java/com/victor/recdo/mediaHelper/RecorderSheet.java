package com.victor.recdo.mediaHelper;

import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.victor.recdo.R;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Objects;

public class RecorderSheet extends BottomSheetDialogFragment implements View.OnClickListener, Runnable {
    public static final String ARG_STRING = "com.victor.recdo.mediaHelper.ARG_STRING";
    private static final int ACTION_RECORDING = 1;
    private static final int ACTION_NORMAL = 0;
    private static final int ACTION_COMPLETE = 2;
    private static final int ACTION_PLAYING = 3;
    private static final int ACTION_PAUSE = 4;
    private static int mCurrentActionState = ACTION_NORMAL;
    private TextView mTvRecorderTime;
    private ImageView mIvAction;
    private ImageView mCircle;
    private RelativeLayout mRlBottom;
    private MediaRecorderHelper mMediaRecorderHelper;
    private int mTotalSecond = 0;
    private boolean mIsRecorder;
    private String mRecorderPath;
    private SenderInterface mRecorderCallBack;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recorder_sheet, container, false);
        initView(view);

        Bundle bundle = this.getArguments();
        assert bundle != null;
        mRecorderPath = bundle.getString(ARG_STRING);
        Toast.makeText(getActivity(), mRecorderPath, Toast.LENGTH_SHORT).show();
        if (mRecorderPath != null) {
            mCurrentActionState = ACTION_COMPLETE;
            mIvAction.setImageResource(R.drawable.icon_audio_state_uploaded);
            mIvAction.setOnClickListener(v -> switchActionState());
            mRlBottom.setVisibility(View.VISIBLE);
        } else {
            mMediaRecorderHelper = new MediaRecorderHelper(getRecorderFilePath());
            mIsRecorder = false;
            changeToNormalState();
        }

        return view;
    }


    public void setRecorderCallBack(SenderInterface callback) {
        this.mRecorderCallBack = callback;
    }

    private void initView(@NotNull View view) {
        mTvRecorderTime = view.findViewById(R.id.tv_recorder_time);
        mIvAction = view.findViewById(R.id.iv_action);
        mIvAction.setOnClickListener(this);
        mRlBottom = view.findViewById(R.id.rl_bottom);
        mRlBottom.setVisibility(View.GONE);
        ImageView mIvDelete = view.findViewById(R.id.iv_delete);
        ImageView mIvSave = view.findViewById(R.id.iv_save);
        mCircle = view.findViewById(R.id.rec_circle);
        mIvDelete.setOnClickListener(this);
        mIvSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_action:
                switchActionState();
                break;
            case R.id.iv_delete:
                changeToNormalState();
                if (mRecorderPath != null) {
                    File file = new File(mRecorderPath);
                    file.delete();
//                    mRecorderPath = null;
                    mRecorderCallBack.receivedLink(null);
                } else {
                    File file = new File(mMediaRecorderHelper.getCurrentFilePath());
                    file.delete();
                }
                dismiss();
                break;
            case R.id.iv_save:
                changeToNormalState();
                if (mRecorderPath == null) {
                    mRecorderCallBack.receivedLink(mMediaRecorderHelper.getCurrentFilePath());
                }
                dismiss();
                break;
            default:
                break;
        }
    }

    @NonNull
    public String getRecorderFilePath() {
        String path = "";
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            try {
                path = Objects.requireNonNull(Objects.requireNonNull(getActivity()).getExternalCacheDir()).getAbsolutePath();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            path = Objects.requireNonNull(getActivity()).getCacheDir().getAbsolutePath();
        }
        return path + File.separator + "Recorder";
    }

    private void switchActionState() {
        mIsRecorder = false;
        if (mCurrentActionState == ACTION_NORMAL) {
            mCurrentActionState = ACTION_RECORDING;
            mIvAction.setImageResource(R.drawable.stop);
            mCircle.setImageResource(R.drawable.circle_red);

            mMediaRecorderHelper.startRecord();
            mIsRecorder = true;
            Thread mCountTimeThread = new Thread(this);
            mCountTimeThread.start();
        } else if (mCurrentActionState == ACTION_RECORDING) {
            mCurrentActionState = ACTION_COMPLETE;
            mIvAction.setImageResource(R.drawable.icon_audio_state_uploaded);
            mCircle.setImageResource(R.drawable.circle_black);
            mMediaRecorderHelper.stopAndRelease();
            mRlBottom.setVisibility(View.VISIBLE);
        } else if (mCurrentActionState == ACTION_COMPLETE) {
            mCurrentActionState = ACTION_PLAYING;
            mIvAction.setImageResource(R.drawable.icon_audio_state_uploaded_play);
            mCircle.setImageResource(R.drawable.circle_green);
            MediaPlayerHelper.playSound((mRecorderPath != null) ? mRecorderPath : mMediaRecorderHelper.getCurrentFilePath(), mp -> {
                mCurrentActionState = ACTION_COMPLETE;
                mIvAction.setImageResource(R.drawable.icon_audio_state_uploaded);
                mCircle.setImageResource(R.drawable.circle_black);
            });
        } else if (mCurrentActionState == ACTION_PLAYING) {
            mCurrentActionState = ACTION_PAUSE;
            mIvAction.setImageResource(R.drawable.icon_audio_state_uploaded);
            MediaPlayerHelper.pause();
        } else if (mCurrentActionState == ACTION_PAUSE) {
            mCurrentActionState = ACTION_PLAYING;
            mIvAction.setImageResource(R.drawable.icon_audio_state_uploaded_play);
            MediaPlayerHelper.resume();
        }
    }

    public void changeToNormalState() {
        MediaPlayerHelper.release();
        mCurrentActionState = ACTION_NORMAL;
        mIvAction.setImageResource(R.drawable.btn_clue_audio);
        mTotalSecond = 0;
        mTvRecorderTime.setText(getString(R.string.timeRec));
        mRlBottom.setVisibility(View.GONE);
        mCircle.setImageResource(R.drawable.circle_black);
    }

    @Contract(pure = true)
    private String getShowTime(int countTime) {
        String result = "";
        if (countTime < 10) {
            result = "00:0" + countTime;
        } else if (countTime < 60) {
            result = "00:" + countTime;
        } else {
            int minute = countTime / 60;
            int mod = countTime % 60;
            if (minute < 10) {
                result += "0" + minute + ":";
            } else {
                result += minute + ":";
            }
            if (mod < 10) {
                result += "0" + mod;
            } else {
                result += mod;
            }
        }
        return result;
    }

    @Override
    public void run() {
        while (mIsRecorder) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mTotalSecond++;

            if (getActivity() == null) {
                return;
            }

            getActivity().runOnUiThread(() -> mTvRecorderTime.setText(getShowTime(mTotalSecond)));
        }
    }

    public interface SenderInterface {
        void receivedLink(String link);
    }

}
