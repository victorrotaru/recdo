package com.victor.recdo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.victor.recdo.adapters.ImageAdapter;
import com.victor.recdo.mediaHelper.RecorderSheet;
import com.victor.recdo.reminder.ReminderSheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class AddEditNote extends AppCompatActivity implements ImageAdapter.ItemClickListener, RecorderSheet.SenderInterface, ReminderSheet.ReminderInterface {
    private static final int READ_REQUEST_CODE = 1;
    private static final String emptyStr = "Note";
    private final StringBuilder mSb = new StringBuilder();
    public TextView mShowReminder;
    private RecyclerView mRecyclerView;
    private EditText mEtNote;
    private ImageAdapter mAdapter;
    private String mRecordPath;
    private String mDateTime;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_reminder:
                ReminderSheet reminderSheet = new ReminderSheet();
                Bundle reminderArgs = new Bundle();
                reminderArgs.putString(ReminderSheet.ARG_REMINDER, mDateTime);
                reminderSheet.setArguments(reminderArgs);
                reminderSheet.show(getSupportFragmentManager(), "ReminderSheet");
                return true;
            case R.id.navigation_recorder:
                RecorderSheet bottomSheet = new RecorderSheet();
                Bundle recorderArgs = new Bundle();
                recorderArgs.putString(RecorderSheet.ARG_STRING, mRecordPath);
                bottomSheet.setArguments(recorderArgs);
                bottomSheet.show(getSupportFragmentManager(), "RecorderSheet");
                Toast.makeText(this, mDateTime, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.navigation_image:
                getImageFromAlbum();
                return true;
        }
        return false;
    };

    public static String getDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_note);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mEtNote = findViewById(R.id.etNote);
        mShowReminder = findViewById(R.id.show_reminder);


        adapterDec();
        try {
            getExtraIntent();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof RecorderSheet) {
            RecorderSheet recorderSheet = (RecorderSheet) fragment;
            recorderSheet.setRecorderCallBack(this);
        } else if (fragment instanceof ReminderSheet) {
            ReminderSheet reminderSheet = (ReminderSheet) fragment;
            reminderSheet.setReminderCallBack(this);
        }
    }

    @Override
    public void receivedLink(String link) {
        mRecordPath = link;
    }

    private void getExtraIntent() throws ParseException {
        Intent intent = getIntent();
        if (intent.hasExtra(MainActivity.EXTRA_ID)) {
            setTitle("Edit Note");
            mEtNote.setText(intent.getStringExtra(MainActivity.EXTRA_NOTE));
            mRecordPath = intent.getStringExtra(MainActivity.EXTRA_RECORD);
            String image = intent.getStringExtra(MainActivity.EXTRA_IMAGE);
            mAdapter.images.addAll(Arrays.asList(image.split("\t")));
            mAdapter.notifyDataSetChanged();
            // reminder
            mDateTime = intent.getStringExtra(MainActivity.EXTRA_REMINDER);
            dateChecker();
        } else {
            setTitle("Add Note");
        }
    }

    private void dateChecker() throws ParseException {
        if (mDateTime != null) {
            long dateTimeL = Long.parseLong(mDateTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(dateTimeL);
            if (calendar.after(Calendar.getInstance())) {
                Toast.makeText(this, "After", Toast.LENGTH_LONG).show();
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                Date date = fmt.parse(mDateTime);
                mShowReminder.setText(date.toString());

            } else if (calendar.before(Calendar.getInstance())) {
                Toast.makeText(this, "Before", Toast.LENGTH_SHORT).show();
                mDateTime = null;
            }
        } else {
            Toast.makeText(this, "Null mDateTime", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onItemClick(int position) {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            Snackbar.make(this, R.string.permission_storage_rationale,
//                    Snackbar.LENGTH_INDEFINITE)
//                    .setAction(R.string.ok, view -> ActivityCompat.requestPermissions(this,
//                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                            MainActivity.REQUEST_STORAGE))
//                    .show();
//
//        } else {
        Uri uri = Uri.parse(mAdapter.getItem(position));
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
//        }
    }

    private void getImageFromAlbum() {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED) {
//                Snackbar.make(mImageLayout, R.string.permission_storage_rationale,
//                        Snackbar.LENGTH_INDEFINITE)
//                        .setAction(R.string.ok, v -> openSettings())
//                        .show();
//            } else {
//                Snackbar.make(mImageLayout, R.string.permission_storage_rationale,
//                        Snackbar.LENGTH_INDEFINITE)
//                        .setAction(R.string.ok, view -> ActivityCompat.requestPermissions(this,
//                                new String[]{Manifest.permission.RECORD_AUDIO},
//                                MainActivity.REQUEST_RECORD))
//                        .show();
//            }
//        } else {
        Intent intent;
        intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.form_pick_images)), READ_REQUEST_CODE);
//        }
    }

    public void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void adapterDec() {
        mRecyclerView = findViewById(R.id.imageRecycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new ImageAdapter();
        mAdapter.setClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        touchHelper();
    }

    private void touchHelper() {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                mAdapter.images.remove(mAdapter.getItem(viewHolder.getAdapterPosition()));
                mAdapter.notifyDataSetChanged();
            }
        }).attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void mDateTimeSender(String dateTime) {
        mDateTime = dateTime;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == READ_REQUEST_CODE && resultCode == AddEditNote.RESULT_OK) {
            Uri imageUri = data.getData();
            getContentResolver().takePersistableUriPermission(Objects.requireNonNull(imageUri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (mAdapter.images.contains(imageUri.toString())) {
                Toast.makeText(this, "Image already exists", Toast.LENGTH_SHORT).show();
            } else {
                mSb.append(imageUri.toString());
                mSb.append("\t");
                mAdapter.images.add(imageUri.toString());
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void saveNote() {
        String note = mEtNote.getText().toString();
        StringBuilder builder = new StringBuilder();
        for (String s : mAdapter.images) {
            builder.append(s);
            builder.append("\t");
        }
        String str = builder.toString();
        if (TextUtils.isEmpty(note)) {
            note = emptyStr;
        }

        Intent data = new Intent();
        data.putExtra(MainActivity.EXTRA_NOTE, note);
        data.putExtra(MainActivity.EXTRA_RECORD, mRecordPath);
        data.putExtra(MainActivity.EXTRA_IMAGE, str);
        data.putExtra(MainActivity.EXTRA_REMINDER, mDateTime);
        Toast.makeText(this, mDateTime, Toast.LENGTH_SHORT).show();

        int id = getIntent().getIntExtra(MainActivity.EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(MainActivity.EXTRA_ID, id);
        }


        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        saveNote();
        return true;
    }

    @Override
    public void onBackPressed() {
        saveNote();
        super.onBackPressed();
    }
}
